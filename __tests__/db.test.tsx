import mysql from 'serverless-mysql';
import excuteQuery from "@/lib/db";

jest.mock('serverless-mysql', () => {
    const mSql = {
        query: jest.fn(),
        end: jest.fn()
    };
    return jest.fn(() => mSql);
});

const db = mysql() as jest.Mocked<ReturnType<typeof mysql>>;

describe('db', () => {

    afterEach(() => {
        jest.clearAllMocks();
    });

    test('should execute query and return results', async () => {
        const mockResults = [{ id: 1, name: 'Test' }];
        db.query.mockResolvedValueOnce(mockResults);

        const query = 'SELECT * FROM users';
        const values: any[] = [];

        const results = await excuteQuery({ query, values });

        expect(db.query).toHaveBeenCalledWith(query, values);
        expect(db.end).toHaveBeenCalled();
        expect(results).toEqual(mockResults);
    });

    test('should throw an error when query fails', async () => {
        const mockError = new Error('Query failed');
        jest.spyOn(console, 'error').mockImplementation(() => {});

        db.query.mockRejectedValueOnce(mockError);

        const query = 'SELECT * FROM users';
        const values: any[] = [];

        await expect(excuteQuery({ query, values })).rejects.toThrow('Query execution failed');

        expect(db.query).toHaveBeenCalledWith(query, values);
        expect(db.end).toHaveBeenCalled();
        expect(console.error).toHaveBeenCalledWith(mockError);
    });
});
