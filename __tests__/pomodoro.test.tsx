import React from 'react';
import { render, screen, fireEvent, act } from '@testing-library/react';
import Pomodoro from "@/components/pomodoro";
import '@testing-library/jest-dom';
import confetti from 'canvas-confetti';

jest.mock('canvas-confetti', () => {
    const mockConfetti: jest.Mocked<typeof confetti> = jest.fn() as any;
    mockConfetti.shapeFromText = jest.fn().mockReturnValue('dummyShape');
    return mockConfetti;
});

beforeAll(() => {
    Object.defineProperty(HTMLMediaElement.prototype, 'play', {
        configurable: true,
        value: jest.fn().mockImplementation(() => {
            console.log("Mocked play function called");
        }),
    });
});

beforeEach(() => {
    jest.useFakeTimers();
    jest.spyOn(global, 'clearInterval');
});

afterEach(() => {
    jest.clearAllTimers();
    jest.clearAllMocks();
});

test('renders the pomodoro timer correctly', () => {
    render(<Pomodoro history={[]} callback={() => {}} />);

    const timerDisplay = screen.getByText(/Working/i);
    const timeRemainingDisplay = screen.getByText(/10:00|15:00|25:00|45:00/i);
    const startButton = screen.getByRole('button', { name: /Start/i });

    expect(timerDisplay).toBeInTheDocument();
    expect(timeRemainingDisplay).toBeInTheDocument();
    expect(startButton).toBeInTheDocument();
});

test('starts the timer on button click', () => {
    render(<Pomodoro history={[]} callback={() => {}} />);

    const startButton = screen.getByRole('button', { name: /Start/i });
    fireEvent.click(startButton);

    expect(clearInterval).not.toHaveBeenCalled();

    act(() => {
        jest.advanceTimersByTime(1000);
    });

    expect(clearInterval).toHaveBeenCalledTimes(1);
});

test('preset buttons adjust the timer config', () => {
    render(<Pomodoro history={[]} callback={() => {}} />);

    const devModeButton = screen.getByRole('button', { name: /Dev mode/i });
    fireEvent.click(devModeButton);

    const timeRemainingDisplayDev = screen.getByText(/0?:10/i);
    expect(timeRemainingDisplayDev).toBeInTheDocument();

    const presetButton15 = screen.getByRole('button', { name: /15\/5/i });
    fireEvent.click(presetButton15);

    const timeRemainingDisplay15 = screen.getByText(/15:00/i);
    expect(timeRemainingDisplay15).toBeInTheDocument();
});

test('history section displays correct data', () => {
    const historyMock = [
        { id: 1, work_time: 10, break_time: 5, streak: 1, date_created: new Date() },
    ];

    render(<Pomodoro history={historyMock} callback={() => {}} />);

    const historyButton = screen.getByText(/History/i);
    fireEvent.click(historyButton);

    const historyItem = screen.getByText(/Work Time: 10 seconds/i);

    expect(historyItem).toBeInTheDocument();
});

test('should reset the timer correctly when forceReset is true', () => {
    render(<Pomodoro history={[]} callback={() => {}} />);

    const resetButton = screen.getByRole('button', { name: /Reset/i });
    fireEvent.click(resetButton);

    const timeRemainingDisplay = screen.getByText(/45:00/i);
    expect(timeRemainingDisplay).toBeInTheDocument();
    expect(screen.queryByText(/Streak :/)).not.toBeInTheDocument();
});

test('should trigger confetti and play audio at the end of a working session', () => {
    const playMock = jest.fn();
    (HTMLAudioElement.prototype.play as jest.Mock).mockImplementation(playMock);

    render(<Pomodoro history={[]} callback={() => {}} />);

    const startButton = screen.getByRole('button', { name: /Start/i });
    fireEvent.click(startButton);

    act(() => {
        jest.advanceTimersByTime(45 * 60 * 1000);
    });

    expect(confetti).toHaveBeenCalledTimes(2);
    expect(playMock).toHaveBeenCalled();
    expect(screen.getByText(/Chill/i)).toBeInTheDocument();
});
